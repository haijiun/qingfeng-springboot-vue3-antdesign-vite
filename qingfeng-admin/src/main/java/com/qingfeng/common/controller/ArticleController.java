package com.qingfeng.common.controller;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.qingfeng.base.controller.BaseController;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.common.entity.Article;
import com.qingfeng.common.entity.Demo;
import com.qingfeng.common.service.IArticleService;
import com.qingfeng.common.service.IDemoService;
import com.qingfeng.framework.exception.BizException;
import com.qingfeng.system.entity.UserOrganize;
import com.qingfeng.system.service.IUserOrganizeService;
import com.qingfeng.system.service.IUserService;
import com.qingfeng.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@Validated
@RestController
@RequestMapping("/common/article")
public class ArticleController extends BaseController {

    @Autowired
    private IArticleService articleService;


    /**
     * 保存数据
     * @param article
     * @param response
     * @throws Exception
     */
    @PostMapping
    public void save(@Valid @RequestBody Article article, HttpServletResponse response) throws Exception {
        Json json = new Json();
        try {
            // 创建用户
            String id = GuidUtil.getUuid();
            article.setId(id);
            Article artic = this.articleService.create(article);
            json.setSuccess(true);
            json.setMsg("新增信息成功");
            json.setData(artic);
        } catch (Exception e) {
            String message = "新增信息失败";
            json.setSuccess(false);
            json.setMsg(message);
            log.error(message, e);
            throw new BizException(message);
        }
        this.writeJson(response,json);
    }


    /**
     * 删除数据
     * @param id
     * @param response
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public void delete(@NotBlank(message = "{required}") @PathVariable String id,HttpServletResponse response) throws Exception {
        Json json = new Json();
        try {
            this.articleService.delete(id);
            json.setSuccess(true);
            json.setMsg("删除信息成功");
        } catch (Exception e) {
            String message = "删除信息失败";
            json.setSuccess(false);
            json.setMsg(message);
            log.error(message, e);
            throw new BizException(message);
        }
        this.writeJson(response,json);
    }


    /**
     * 查询详情
     * @param article
     * @return
     * @throws IOException
     */
    @GetMapping("/info")
    public MyResponse list(Article article) throws IOException  {
        Article artic = articleService.get(article.getId());
        return new MyResponse().data(artic);
    }

}
