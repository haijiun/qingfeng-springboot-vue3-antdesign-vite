package com.qingfeng.common.service.impl;

import com.qingfeng.common.entity.Article;
import com.qingfeng.common.service.IArticleService;
import org.apache.catalina.User;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName TdemoServiceImpl
 * @author Administrator
 * @version 1.0.0
 * @Description TdemoServiceImpl接口
 * @createTime 2022/1/19 0019 22:55
 */
@Service
public class ArticleServiceImpl implements IArticleService {

    @Autowired
    private MongoTemplate mongoTemplate;


    public List<Article> find(Article article) {
        //  查询name=zs
        Query query = Query.query(Criteria.where("name").is("zs"));
        List<Article> list = mongoTemplate.find(query,Article.class);
        List<Article> list1 = mongoTemplate.find(query,Article.class,"qingfeng");
        System.out.println(list);
        System.out.println(list1);
        return list1;
    }

    public List<Article> findAll(Article article) {
        //  查询所有
        List<Article> list = mongoTemplate.findAll(Article.class);
        List<Article> list1 = mongoTemplate.findAll(Article.class,"qingfeng");
        System.out.println(list);
        System.out.println(list1);
        return list1;
    }

    public List<Article> findPage(Article article,int page,int pageSize) {
        //  分页查询	page页码，pageSize每页展示几个
        Pageable pageable = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("date")));
        Query query = new Query().with(pageable);
        return this.mongoTemplate.find(query, Article.class,"qingfeng");
    }


    public List<Article> findIn(Article article) {
        //  查询多个
        Query query= Query.query(Criteria.where("id").in("id1","id2","id3")).with(Sort.by(Sort.Order.desc("id")));
        List<Article> list= this.mongoTemplate.find(query, Article.class);
        return list;
    }


    public long findCount(Article article) {
        //  查询数量
        Criteria criteria = Criteria.where("userId").is("12345")
                .and("name").is(new ObjectId("张三"))
                .and("address").is("上海");
        Query query = Query.query(criteria);
        long count = this.mongoTemplate.count(query, Article.class);
        return count;
    }


    public void save() {
        List<Article> list = new ArrayList<>();
        Article article= new Article();//
        article.setArticleName("admin");
        article.setContent("测试");
        list.add(article);

        //  保存对象到mongodb
        mongoTemplate.save(article);
        mongoTemplate.insert(article);
        //  根据集合名称保存对象到mongodb
        mongoTemplate.save(article,"mongodb_user");
        mongoTemplate.insert(article,"mongodb_user");
        //  根据集合名称保存list到mongodb
        mongoTemplate.save(list,"mongodb_user");
        mongoTemplate.insert(list,"mongodb_user");
        mongoTemplate.insert(list, User.class);
    }


    public void update() {
        Article article = new Article();
        article.setId("5d1312aeb1829c279c6c256b");
        article.setArticleName("admin");
        article.setContent("测试");

        Query query = Query.query(Criteria.where("_id").is("5d1312aeb1829c279c6c256b"));
        Update update = Update.update("name","zs");
        //  更新一条数据
        mongoTemplate.updateFirst(query,update, Article.class);
        mongoTemplate.updateFirst(query,update, "mongodb_user");
        mongoTemplate.updateFirst(query,update, Article.class,"mongodb_user");
        //  更新多条数据
        mongoTemplate.updateMulti(query,update, Article.class);
        mongoTemplate.updateMulti(query,update,"mongodb_user");
        mongoTemplate.updateMulti(query,update, Article.class,"mongodb_user");
        //  更新数据，如果数据不存在就新增
        mongoTemplate.upsert(query,update, Article.class);
        mongoTemplate.upsert(query,update,"mongodb_user");
        mongoTemplate.upsert(query,update, Article.class,"mongodb_user");
    }

    public void del() {
        List<Article> list = new ArrayList<>();
        Article article= new Article();
        article.setId("5d1312aeb1829c279c6c256b");
        list.add(article);

        Query query = Query.query(Criteria.where("_id").in("5d1312aeb1829c279c6c256b","5d13133ab1829c29d02ce29c"));
        //  根据条件删除
        mongoTemplate.remove(query);
        mongoTemplate.remove(article);
        mongoTemplate.remove(User.class);
        //  根据条件删除（可删除多条）
        mongoTemplate.remove(query,User.class,"mongodb_user");
    }





    /**
     * 添加文章
     * @param article
     * @return
     */
    public Article create(Article article) {
        Article save = mongoTemplate.save(article);
        return save;
    }

    /**
     * 删除文章
     * @param id
     */
    public void delete(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query,Article.class);
    }

    /**
     * 查询
     * @param id
     * @return
     */
    public Article get(String id) {
        Article article = mongoTemplate.findById(id, Article.class);
        return article;
    }

}