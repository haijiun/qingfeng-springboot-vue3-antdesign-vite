package com.qingfeng.screen.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.qingfeng.base.controller.BaseController;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.framework.exception.BizException;
import com.qingfeng.screen.entity.VisualCategory;
import com.qingfeng.screen.service.IVisualCategoryService;
import com.qingfeng.utils.MyResponse;
import com.qingfeng.utils.MyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @ProjectName VisualCategoryController
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 14:58
 */
@Slf4j
@Validated
@RestController
@RequestMapping("category")
public class VisualCategoryController extends BaseController {

    @Autowired
    private IVisualCategoryService visualCategoryService;

    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:51
     */
    @GetMapping("/list")
    public MyResponse findListPage(QueryRequest queryRequest, VisualCategory visualCategory) {
        String userParams = SecurityContextHolder.getContext().getAuthentication().getName();
//        Map<String, Object> dataTable = MyUtil.getDataTable(visualCategoryService.findListPage(visualCategory, queryRequest));
        return new MyResponse().data(visualCategoryService.findListPage(visualCategory, queryRequest));
    }

    /**
     * @title detail
     * @description 查询详情信息
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:51
     */
    @GetMapping("/detail")
    public MyResponse detail(VisualCategory visualCategory) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id",visualCategory.getId());
        VisualCategory detail = visualCategoryService.getOne(queryWrapper);
        return new MyResponse().data(detail);
    }

    /**
     * @title findList
     * @description 查询信息列表
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:51
     */
    @GetMapping("/findList")
    public MyResponse findList(VisualCategory visualCategory) {
        String userParams = SecurityContextHolder.getContext().getAuthentication().getName();
        List<VisualCategory> categoryList = visualCategoryService.findList(visualCategory);
        return new MyResponse().data(categoryList);
    }



    /**
     * @title save
     * @description 保存数据
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:52
     */
    @PostMapping("/save")
    public void save(@Valid @RequestBody VisualCategory visualCategory) throws BizException {
        try {
            this.visualCategoryService.saveVisualCategory(visualCategory);
        } catch (Exception e) {
            String message = "新增信息失败";
            log.error(message, e);
            throw new BizException(message);
        }
    }

    /**
     * @title update
     * @description 更新数据
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:52
     */
    @PostMapping("/update")
    public void update(@Valid @RequestBody VisualCategory visualCategory) throws BizException {
        try {
            this.visualCategoryService.updateVisualCategory(visualCategory);
        } catch (Exception e) {
            String message = "修改信息失败";
            log.error(message, e);
            throw new BizException(message);
        }
    }

    /**
     * @title delete
     * @description 删除数据
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:52
     */
    @DeleteMapping("/{ids}")
    public void remove(@NotBlank(message = "{required}") @PathVariable String ids) throws BizException {
        try {
            String[] del_ids = ids.split(StringPool.COMMA);
            this.visualCategoryService.removeByIds(Arrays.asList(del_ids));
        } catch (Exception e) {
            String message = "删除失败";
            log.error(message, e);
            throw new BizException(message);
        }
    }


}
