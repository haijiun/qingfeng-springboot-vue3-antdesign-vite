package com.qingfeng.screen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @ProjectName VisualConfig
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 12:07
 */
@Data
@TableName("bigscreen_visual_config")
public class VisualConfig implements Serializable {

    private static final long serialVersionUID = -4352868070794165001L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 可视化表主键
     */
    @TableField("visual_id")
    private String visualId;

    /**
     * 配置json
     */
    @TableField("detail")
    private String detail;
    /**
     * 组件json
     */
    @TableField("component")
    private String component;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private String createTime;
    /**
     * 创建人
     */
    @TableField("create_user")
    private String createUser;
    /**
     * 创建组织
     */
    @TableField("create_organize")
    private String createOrganize;

    /**
     * 修改人
     */
    @TableField("update_user")
    private String updateUser;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private String updateTime;

}