package com.qingfeng.screen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @ProjectName VisualRecord
 * @author Administrator
 * @version 1.0.0
 * @Description 数据集管理
 * @createTime 2022/11/19 0019 23:50
 */
@Data
@TableName("bigscreen_visual_record")
public class VisualRecord implements Serializable {

    private static final long serialVersionUID = -4352868070794165001L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 接口地址
     */
    @TableField("url")
    private String url;

    /**
     * 数据类型
     */
    @TableField("dataType")
    private String dataType;
    /**
     * 请求方法
     */
    @TableField("dataMethod")
    private String dataMethod;
    /**
     * 请求头header
     */
    @TableField("dataHeader")
    private String dataHeader;
    /**
     * 数据
     */
    @TableField("data")
    private String data;
    /**
     * 查询
     */
    @TableField("dataQuery")
    private String dataQuery;
    /**
     * 查询类型
     */
    @TableField("dataQueryType")
    private String dataQueryType;
    /**
     * 格式化的数据
     */
    @TableField("dataFormatter")
    private String dataFormatter;
    /**
     * webservice地址
     */
    @TableField("wsUrl")
    private String wsUrl;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private String createTime;
    /**
     * 创建人
     */
    @TableField("create_user")
    private String createUser;
    /**
     * 创建组织
     */
    @TableField("create_organize")
    private String createOrganize;

    /**
     * 修改人
     */
    @TableField("update_user")
    private String updateUser;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private String updateTime;

}