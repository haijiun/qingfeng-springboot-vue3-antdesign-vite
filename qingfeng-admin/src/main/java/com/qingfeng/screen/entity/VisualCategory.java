package com.qingfeng.screen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @ProjectName VisualCategory
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 11:42
 */
@Data
@TableName("bigscreen_visual_category")
public class VisualCategory implements Serializable {

    private static final long serialVersionUID = -4352868070794165001L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 分类键值
     */
    @TableField("category_key")
    private String categoryKey;

    /**
     * 分类名称
     */
    @TableField("category_value")
    private String categoryValue;
    /**
     * 是否删除
     */
    @TableField("is_deleted")
    private String isDeleted;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private String createTime;
    /**
     * 创建人
     */
    @TableField("create_user")
    private String createUser;
    /**
     * 创建组织
     */
    @TableField("create_organize")
    private String createOrganize;

    /**
     * 修改人
     */
    @TableField("update_user")
    private String updateUser;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private String updateTime;

}