package com.qingfeng.screen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.screen.entity.VisualComponent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ProjectName VisualComponentMapper
 * @author Administrator
 * @version 1.0.0
 * @Description VisualComponentMapper
 * @createTime 2022/11/20 0020 0:02
 */
public interface VisualComponentMapper extends BaseMapper<VisualComponent> {

    //查询数据分页列表
    IPage<VisualComponent> findListPage(Page page, @Param("obj") VisualComponent visualComponent);

    //查询数据列表
    List<VisualComponent> findList(VisualComponent visualComponent);

}
