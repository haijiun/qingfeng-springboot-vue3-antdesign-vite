package com.qingfeng.screen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.screen.entity.VisualRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ProjectName VisualRecordMapper
 * @author Administrator
 * @version 1.0.0
 * @Description VisualRecordMapper
 * @createTime 2022/11/20 0020 0:01
 */
public interface VisualRecordMapper extends BaseMapper<VisualRecord> {

    //查询数据分页列表
    IPage<VisualRecord> findListPage(Page page, @Param("obj") VisualRecord visualRecord);

    //查询数据列表
    List<VisualRecord> findList(VisualRecord visualRecord);

}
