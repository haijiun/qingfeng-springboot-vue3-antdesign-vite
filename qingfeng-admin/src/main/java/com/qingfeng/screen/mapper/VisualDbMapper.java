package com.qingfeng.screen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.screen.entity.VisualDb;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ProjectName VisualDbMapper
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/19 0019 17:17
 */
public interface VisualDbMapper extends BaseMapper<VisualDb> {

    //查询数据分页列表
    IPage<VisualDb> findListPage(Page page, @Param("obj") VisualDb visualDb);

    //查询数据列表
    List<VisualDb> findList(VisualDb visualDb);

}
