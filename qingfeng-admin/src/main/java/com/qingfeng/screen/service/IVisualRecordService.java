package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualRecord;

import java.util.List;

/**
 * @ProjectName IVisualRecordService
 * @author Administrator
 * @version 1.0.0
 * @Description IVisualRecordService
 * @createTime 2022/11/20 0020 0:06
 */
public interface IVisualRecordService extends IService<VisualRecord> {

    //查询数据分页列表
    IPage<VisualRecord> findListPage(VisualRecord visualRecord, QueryRequest request);

    //查询数据列表
    List<VisualRecord> findList(VisualRecord visualRecord);

    //保存信息
    void saveVisualMap(VisualRecord visualRecord);

    //更新信息
    void updateVisualMap(VisualRecord visualRecord);

}