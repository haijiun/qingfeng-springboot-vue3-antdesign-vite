package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualDb;
import com.qingfeng.screen.mapper.VisualDbMapper;
import com.qingfeng.screen.service.IVisualDbService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName GroupServiceImpl
 * @author qingfeng
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/3 0003 21:33
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualDbServiceImpl extends ServiceImpl<VisualDbMapper, VisualDb> implements IVisualDbService {



    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<VisualDb> findListPage(VisualDb visualDb, QueryRequest request){
        Page<VisualDb> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visualDb);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<VisualDb> findList(VisualDb visualDb){
        return this.baseMapper.findList(visualDb);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public boolean saveVisualDb(VisualDb visualDb){
        String time = DateTimeUtil.getDateTimeStr();
//        String authParams = SecurityContextHolder.getContext().getAuthentication().getName();
        visualDb.setId(GuidUtil.getUuid());
        visualDb.setCreateTime(time);
        visualDb.setIsDeleted("0");
        visualDb.setCreateUser("1");
        visualDb.setCreateOrganize("1");
        boolean tempV = this.save(visualDb);
        return tempV;
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public boolean updateVisualDb(VisualDb visualDb){
        String time = DateTimeUtil.getDateTimeStr();
//        String authParams = SecurityContextHolder.getContext().getAuthentication().getName();
        visualDb.setUpdateTime(time);
        visualDb.setUpdateUser("1");
        this.updateById(visualDb);
        return true;
    }


}