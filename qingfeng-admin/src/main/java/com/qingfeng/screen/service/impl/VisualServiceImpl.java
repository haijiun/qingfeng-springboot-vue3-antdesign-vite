package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.Visual;
import com.qingfeng.screen.entity.VisualConfig;
import com.qingfeng.screen.entity.VisualDTO;
import com.qingfeng.screen.mapper.VisualMapper;
import com.qingfeng.screen.service.IVisualConfigService;
import com.qingfeng.screen.service.IVisualService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName GroupServiceImpl
 * @author qingfeng
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/3 0003 21:33
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualServiceImpl extends ServiceImpl<VisualMapper, Visual> implements IVisualService {


    @Autowired
    public IVisualConfigService visualConfigService;

    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<Visual> findListPage(Visual visual, QueryRequest request){
        Page<Visual> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visual);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<Visual> findList(Visual visual){
        return this.baseMapper.findList(visual);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public boolean saveVisual(VisualDTO dto){
        String time = DateTimeUtil.getDateTimeStr();
//        String authParams = SecurityContextHolder.getContext().getAuthentication().getName();

        Visual visual = dto.getVisual();
        visual.setId(GuidUtil.getUuid());
        visual.setCreateTime(time);
        visual.setStatus("1");
        visual.setIsDeleted("0");
        visual.setCreateUser("1");
        visual.setCreateOrganize("1");
        boolean tempV = this.save(visual);

        VisualConfig visualConfig = dto.getConfig();
        visualConfig.setId(GuidUtil.getUuid());
        visualConfig.setCreateTime(time);
        visualConfig.setCreateUser("1");
        visualConfig.setCreateOrganize("1");
        visualConfig.setVisualId(visual.getId());
        boolean tempVc = visualConfigService.save(visualConfig);
        return tempV && tempVc;
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public boolean updateVisual(VisualDTO dto){
        String time = DateTimeUtil.getDateTimeStr();
//        String authParams = SecurityContextHolder.getContext().getAuthentication().getName();

        Visual visual = dto.getVisual();
        VisualConfig visualConfig = dto.getConfig();
        if (visual != null && visual.getId() != null) {
            visual.setUpdateTime(time);
            visual.setUpdateUser("1");
            this.updateById(visual);
        }
        if (visualConfig != null && visualConfig.getId() != null) {
            visualConfig.setUpdateTime(time);
            visualConfig.setUpdateUser("1");
            visualConfigService.updateById(visualConfig);
        }
        return true;
    }


    public VisualDTO detail(Visual visual){
        Visual vis = this.getById(visual.getId());
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("visual_id",visual.getId());
        VisualConfig visualConfig = visualConfigService.getOne(queryWrapper);
        VisualDTO dto = new VisualDTO();
        dto.setVisual(vis);
        dto.setConfig(visualConfig);
        return dto;
    }

    public String copyVisual(String id){
        Visual visual = this.getById(id);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("visual_id",visual.getId());
        VisualConfig visualConfig = visualConfigService.getOne(queryWrapper);
        if (visual != null && visualConfig != null) {
            visual.setId(null);
            visualConfig.setId(null);
            VisualDTO dto = new VisualDTO();
            dto.setVisual(visual);
            dto.setConfig(visualConfig);
            boolean temp = this.saveVisual(dto);
            if (temp) {
                return visual.getId();
            }
        }
        return null;
    }

}