package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualConfig;

import java.util.List;

/**
 * @ProjectName IVisualConfigService
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 13:33
 */
public interface IVisualConfigService extends IService<VisualConfig> {

    //查询数据分页列表
    IPage<VisualConfig> findListPage(VisualConfig visualConfig, QueryRequest request);

    //查询数据列表
    List<VisualConfig> findList(VisualConfig visualConfig);

    //保存信息
    void saveVisualConfig(VisualConfig visualConfig);

    //更新信息
    void updateVisualConfig(VisualConfig visualConfig);

}