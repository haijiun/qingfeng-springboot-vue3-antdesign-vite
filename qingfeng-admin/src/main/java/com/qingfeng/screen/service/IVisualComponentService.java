package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualComponent;
import com.qingfeng.screen.entity.VisualMap;

import java.util.List;

/**
 * @ProjectName IVisualComponentService
 * @author Administrator
 * @version 1.0.0
 * @Description IVisualComponentService
 * @createTime 2022/11/20 0020 0:04
 */
public interface IVisualComponentService extends IService<VisualComponent> {

    //查询数据分页列表
    IPage<VisualComponent> findListPage(VisualComponent visualComponent, QueryRequest request);

    //查询数据列表
    List<VisualComponent> findList(VisualComponent visualComponent);

    //保存信息
    void saveVisualMap(VisualComponent visualComponent);

    //更新信息
    void updateVisualMap(VisualComponent visualComponent);

}