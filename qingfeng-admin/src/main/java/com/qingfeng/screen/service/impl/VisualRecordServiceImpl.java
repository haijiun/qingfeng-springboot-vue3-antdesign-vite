package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualMap;
import com.qingfeng.screen.entity.VisualRecord;
import com.qingfeng.screen.mapper.VisualMapMapper;
import com.qingfeng.screen.mapper.VisualRecordMapper;
import com.qingfeng.screen.service.IVisualMapService;
import com.qingfeng.screen.service.IVisualRecordService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName VisualRecordServiceImpl
 * @author Administrator
 * @version 1.0.0
 * @Description VisualRecordServiceImpl
 * @createTime 2022/11/20 0020 0:08
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualRecordServiceImpl extends ServiceImpl<VisualRecordMapper, VisualRecord> implements IVisualRecordService {


    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<VisualRecord> findListPage(VisualRecord visualRecord, QueryRequest request){
        Page<VisualMap> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visualRecord);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<VisualRecord> findList(VisualRecord visualRecord){
        return this.baseMapper.findList(visualRecord);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void saveVisualMap(VisualRecord visualRecord){
        // 创建用户
        String id = GuidUtil.getUuid();
        visualRecord.setId(id);
        String time = DateTimeUtil.getDateTimeStr();
        visualRecord.setCreateTime(time);
        //处理数据权限
        visualRecord.setCreateUser("1");
        visualRecord.setCreateOrganize("1");
        this.save(visualRecord);
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void updateVisualMap(VisualRecord visualRecord){
        // 更新组织信息
        String time = DateTimeUtil.getDateTimeStr();
        visualRecord.setUpdateTime(time);
        visualRecord.setUpdateUser("1");
        this.updateById(visualRecord);
    }


}