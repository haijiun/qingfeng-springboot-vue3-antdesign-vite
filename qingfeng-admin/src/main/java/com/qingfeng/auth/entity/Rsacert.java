package com.qingfeng.auth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ProjectName Rsacert
 * @author Administrator
 * @version 1.0.0
 * @Description 字典信息
 * @createTime 2022/1/19 0019 22:54
 */
@Data
@TableName("auth_rsacert")
public class Rsacert implements Serializable {

    private static final long serialVersionUID = -4352868070794165001L;

    /**
    * 主键id
    */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;
    /**
    * 类型
    */
    @TableField("type")
    private String type;
    /**
    * 标题
    */
    @TableField("title")
    private String title;
    /**
    * 手机号
    */
    @TableField("phone")
    private String phone;
    /**
    * 联系人
    */
    @TableField("linkUser")
    private String linkUser;
    /**
    * 公钥
    */
    @TableField("publicKey")
    private String publicKey;
    /**
    * 私钥
    */
    @TableField("privateKey")
    private String privateKey;
    /**
    * IP地址
    */
    @TableField("ipAddress")
    private String ipAddress;
    /**
    * Mac地址
    */
    @TableField("macAddress")
    private String macAddress;
    /**
    * CPU序列号
    */
    @TableField("cpuSerial")
    private String cpuSerial;
    /**
    * 主板序列号
    */
    @TableField("mainBoardSerial")
    private String mainBoardSerial;
    /**
    * 开始时间
    */
    @TableField("start_time")
    private String start_time;
    /**
    * 结束时间
    */
    @TableField("end_time")
    private String end_time;
    /**
    * 用户数量
    */
    @TableField("user_num")
    private String user_num;
    /**
    * 状态
    */
    @TableField("status")
    private String status;
    /**
    * 排序
    */
    @TableField("order_by")
    private String order_by;
    /**
    * 备注
    */
    @TableField("remark")
    private String remark;
    /**
    * 创建时间
    */
    @TableField("create_time")
    private String create_time;
    /**
    * 创建人
    */
    @TableField("create_user")
    private String create_user;
    /**
     * 创建组织
     */
    @TableField("create_organize")
    private String create_organize;

    /**
    * 修改人
    */
    @TableField("update_user")
    private String update_user;
    /**
    * 修改时间
    */
    @TableField("update_time")
    private String update_time;

    @TableField(exist = false)
    private List<String> auth_organize_ids;

    @TableField(exist = false)
    private String auth_user;

    @TableField(exist = false)
    private String ids;

    @TableField(exist = false)
    private String child_num;
}