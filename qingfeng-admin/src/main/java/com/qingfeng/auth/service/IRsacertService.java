package com.qingfeng.auth.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.auth.entity.Rsacert;

import java.util.List;

/**
 * @ProjectName IRsacertService
 * @author Administrator
 * @version 1.0.0
 * @Description IRsacertService接口
 * @createTime 2022/1/19 0019 22:55
 */
public interface IRsacertService extends IService<Rsacert> {

    //查询数据分页列表
    IPage<Rsacert> findListPage(Rsacert rsacert, QueryRequest request);

    //查询数据列表
    List<Rsacert> findList(Rsacert rsacert);

}